#!/bin/bash
# Basic Settings
localPath="/mnt/NAS/Developement/ant.lgbt (Hugo)/"
serverPath=/var/www/ant.lgbt/root
pullFromGit=true

if [ "$1" = "help" ]
then
	echo Usage: ./upload.sh [help]
	echo "Warning! This will errase your files in the target"
	echo "         directory, check your configs if they're correct"
else
	cd  "$localPath"

	if [ "$pullFromGit" = true ]; then
		echo pull newest version from git
		git pull
	fi

	echo deleting previously generated site...
	rm -rf public

	echo generation site...
	hugo

	echo deleting previous site files...
	rm -R $serverPath/*

	echo copying to server...
	cp -R public/* $serverPath

	echo done
fi
