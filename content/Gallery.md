---
title: "Gallery"
date: 2020-03-15T00:00:00+01:00
draft: false
toc: false
---

As some of you might know, I really like taking pictures. My photography skills aren't professional, to say the lest, but I really enjoy making taking them and experimenting. I hope you'll like some of them anyways.

I also have a Pixabay account with some more pictures of me that are free to use: [pixabay.com](https://pixabay.com/users/confusedant-10937717/)

<!-- https://www.w3schools.com/howto/howto_css_image_grid_responsive.asp -->
<div class="row">
<div class="column">
    <img style="height: calc(100% - 8px)" src="https://stored.ant.lgbt/img/gallery/thumb_me.png" alt="A picture of me, laying on the floor. It's actually quite confortable" onclick="showImageToast('https://stored.ant.lgbt/img/gallery/me.jpg')">
</div>

<div class="column">
    <img src="https://stored.ant.lgbt/img/gallery/thumb_tree.png" onclick="showImageToast('https://stored.ant.lgbt/img/gallery/tree.jpg')">
    <img src="https://stored.ant.lgbt/img/gallery/thumb_sheep.png" onclick="showImageToast('https://stored.ant.lgbt/img/gallery/sheep.jpg')">
</div>
</div>


<div class="row">
<div class="column">
<img src="https://stored.ant.lgbt/img/gallery/hol/IMG_20190621_100105-min.jpg" onclick="showImageToast('https://stored.ant.lgbt/img/gallery/hol/IMG_20190621_100105.jpg')">
<img src="https://stored.ant.lgbt/img/gallery/hol/IMG_20190620_152810-min.jpg" onclick="showImageToast('https://stored.ant.lgbt/img/gallery/hol/IMG_20190620_152810.jpg')">
</div>

<div class="column">
<img src="https://stored.ant.lgbt/img/gallery/hol/IMG_20190620_205527-min.jpg" onclick="showImageToast('https://stored.ant.lgbt/img/gallery/hol/IMG_20190620_205527.jpg')">
</div>
</div>

<script>
document.getElementsByTagName("h1")[0].innerHTML = document.getElementsByTagName("h1")[0].innerHTML + " <i class=\"ri-gallery-fill\"></i>";
</script>

<style>

.row {
  display: flex;
  flex-wrap: wrap;
  padding: 0 4px;
}

/* Create four equal columns that sits next to each other */
.column {
  flex: calc(50% - 8px);
  max-width: calc(50% - 8px);
  padding: 0 4px;
}

.column img {
  margin-top: 8px;
  vertical-align: middle;
  width: 100%;
}

/* Responsive layout - makes the two columns stack on top of each other instead of next to each other */

@media screen and (max-width: 600px) {
  .column {
    flex: 100%;
    max-width: 100%;
  }
}

</style>
