---
title: "Program: Simple GTK Text Editor (Vala)"
date: 2019-12-26T20:31:54+01:00
draft: false
toc: false
categories:
- Developement
tags:
- C
- Vala
- Software
- GTK
- Developement
icon: https://files.ant.lgbt/icons/simpleTextEditor.png
---

This is a very simple text editor I wrote in Vala for GTK. It doesn't have many features, but it can load and save files so you can edit them however you want, I hope you'll have fun with it.

![](https://stored.ant.lgbt/img/simpleTextEditor.png)

This is mearly just a proof of concept, I might (probably) make another text-editor in the future.

> Check out on [Gitlab](https://gitlab.com/ConfusedAnt/texteditor).
