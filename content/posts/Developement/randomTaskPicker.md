---
title: "Program: Random Task Picker (Vala)"
date: 2019-11-20T21:31:41+01:00
draft: false
toc: false
categories:
- Developement
tags:
- C
- Vala
- Software
- GTK
- Developement
icon: https://files.ant.lgbt/icons/randomTaskPicker.png
---

I programmed this small GTK-App to get to know both how to work with GTK and Vala (A programming language, based on C, origionally written for elementary os). It's quite a simple project, but it serves it's purpose.

![](https://stored.ant.lgbt/img/randomTaskPicker.png)

If you have any questions or thoughts about the project, send me an email or message me on Discord.

> Check out on [Gitlab](https://gitlab.com/ConfusedAnt/random-task).
