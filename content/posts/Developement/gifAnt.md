---
title: "Program: GifAnt (JS)"
date: 2020-05-01T12:40:41+02:00
draft: false
toc: false
categories:
- Developement
tags:
- JS
- HTML
- CSS
- Software
- Developement
- Multimedia
icon: https://files.ant.lgbt/icons/gifAnt.png
---

> Disclaimer: I do NOT own the copyright to these gifs, they're just in there in order to function as an example to what this App is capable of.

![](https://files.ant.lgbt/img/gifAnt.png)

GifAnt is a small web-based "Gif-Picker", it shows you a list of available gifs (which you can define in the data file) that you can sort based on tags. It allows you to copy the link of the gif by clicking on it (tested with Firefox and Chrome, some browsers might not work).

> Check out on [Gitlab](https://gitlab.com/ConfusedAnt/gif-ant)
> Or live in the browser [here](https://gif.ant.lgbt/).
