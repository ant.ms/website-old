---
title: "Program: AntChat (PHP)"
date: 2020-03-25T12:00:41+01:00
draft: false
toc: false
categories:
- Developement
tags:
- PHP
- HTML
- CSS
- Software
- Developement
icon: https://files.ant.lgbt/icons/antChat.png
---

Ant Chat is a "as simple as possible" Web-Based (PHP) chat-application. It kinda started as a joke but I reallly enjoyed working on it.

![](https://files.ant.lgbt/img/antChat.png)

> This Application does not have professional security and isn't particularly efficient, use at your own risk

> Check out on [Gitlab](https://gitlab.com/ConfusedAnt/ant-chat)
> Or live in the browser [here](https://projects.ant.lgbt/chat/) (DISLAIMER: Content posted in the chat application is NOT curated and may be dangerous).
