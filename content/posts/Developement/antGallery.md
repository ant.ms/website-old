---
title: "Program: AntGallery (PHP)"
date: 2020-03-31T15:00:41+01:00
draft: false
toc: false
categories:
- Developement
tags:
- PHP
- HTML
- CSS
- Software
- Developement
- Material
- Materializecss
icon: https://files.ant.lgbt/icons/antGallery.png
---

A simple Gallery APP, written in PHP which allows you to upload files. It's really simple, litteraly just one file and a folder with images. It uses the [Materialize](https://materializecss.com/) Framework for the GUI and manages to look quite good in my opinion.

![](https://files.ant.lgbt/img/antGallery.png)

> This Application does not have professional security and isn't particularly efficient, use at your own risk. EVERYONE WITH ACCESS TO THE WEBINTERFACE WILL BE ABLE TO UPLOAD IMAGES

> Check out on [Gitlab](https://gitlab.com/ConfusedAnt/ant-gallery) or live in the browser [here](https://projects.ant.lgbt/simple-gallery/).
