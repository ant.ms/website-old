---
title: "Program: AntReader (PHP)"
date: 2020-03-16T16:00:41+01:00
draft: false
toc: false
categories:
- Developement
tags:
- PHP
- HTML
- CSS
- Software
- Developement
icon: https://files.ant.lgbt/icons/antReader.png
---

AntReader is a simple ebook reader I wrote in PHP. It's design is mainly optimized for mobile but also works quite well on desktop.

![](https://files.ant.lgbt/img/antReader.png)

A very specific filestructire is required for it to function:

```
_books
├── Animal Farm
│   ├── content.md
│   └── cover.png
└── The War of the Worlds
    ├── content.md
    └── cover.png
```

> Check out on [Gitlab](https://gitlab.com/ConfusedAnt/ant-reader).
