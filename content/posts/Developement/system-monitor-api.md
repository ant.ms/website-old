---
title: "Program: System Monitor API (Python)"
date: 2020-07-28T14:40:00+02:00
draft: false
toc: false
categories:
- Developement
tags:
- API
- Python
- Monitor
- Server
- Software
- Developement
#icon: https://stored.ant.lgbt/img/icon_225.png
---

This might not be the coolest ever piece of software, but it certainly can be very useful to have around for when you need it.

It creates a *"REST"*-API, and gives back JSON values that you set before, in order to check on the system remotely (e.g. through a Website/Dashboard). You can see it in action, by going to he [About-Page](/about/), or the [Hosting-Page](/hosting/) on this website.

> Check out on [Gitlab](https://gitlab.com/ConfusedAnt/system-monitor-api)