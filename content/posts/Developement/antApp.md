---
title: "Program: AntApp (Gtk)"
date: 2020-05-04T17:40:00+02:00
draft: false
toc: false
categories:
- Developement
tags:
- Vala
- HTML
- GTK
- Http
- Software
- Developement
icon: https://stored.ant.lgbt/img/icon_225.png
---

This is a simple app that allows the user to view my website, it also includes some additional buttons.

![](https://files.ant.lgbt/img/antApp.png)

The purpose of this application, is to have a "GTK-Native" look on my website. Okay... let's be honest, I just wanted to try it out :)

> Check out on [Gitlab](https://gitlab.com/ConfusedAnt/ant-app)
