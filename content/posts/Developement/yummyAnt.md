---
title: "Program: YummyAnt"
date: 2020-03-24T12:00:54+01:00
draft: false
toc: false
categories:
- Developement
tags:
- HTML
- CSS
- Software
- Material
- Developement
icon: https://files.ant.lgbt/icons/yummyAnt.png
---

YummyAnt is a simple recipe book that I coded in Javascript. It is installable via a service worker (PWA) but doesn't work offline because that could end up using too much storage.

![](https://files.ant.lgbt/img/yummyAnt.png)

For the UI (which I'm kinda proud of, because I'm **horrible** with UI-design) I used [MaterialCSS](https://materializecss.com/). They're freaking awesome and the reason this project looks (at least kind of) professional.

> Check out on [Gitlab](https://gitlab.com/ConfusedAnt/yummy-ant)
> Or open directly in your browser [here](https://yummy.ant.lgbt).
