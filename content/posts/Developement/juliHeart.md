---
title: "Project Leo (Arduino)"
date: 2020-07-02T12:00:41+02:00
draft: false
toc: false
categories:
- Developement
- Project
tags:
- Arduino
- Project
- C
- LED
- Software
- Hardware
- Developement
icon: https://files.ant.lgbt/icons/leo.jpg
---

This was a gift, that I made for my boyfriend for Valentines day

<video controls style="width:100%">
  <source src="https://files.ant.lgbt/img/juliHeart.mp4" type="video/mp4">
</video>

It took me A LOT of work because it was one of the first bigger projects like this, that I have ever done with Arduinos and hardware in general.

The Project consists of a battery, a charging circuit, an Arduino and an LED strip (inside the heart).

> Check out on [Gitlab](https://gitlab.com/ConfusedAnt/project-leo)