---
title: "Game(-ish): 'Stragegy-Like-Camera' (Unity)"
date: 2020-06-15T17:42:12+02:00
draft: false
toc: false
categories:
- Developement
- Games
tags:
- Unity
- Interactive
- C#
- Software
- Developement
- Live
icon: https://files.ant.lgbt/icons/strategyLikeCameraDemo.png
---

This is a demo that I made about camera movement, more specifically, on how to handle strategy-like camera movement in Unity.

<iframe scrolling="no" src="https://projects.ant.lgbt/unity-exportCamera/"></iframe>

As you can see, the demo has some issues and imperfections, it's not meant to be perfect. It serves it's purpose of helping me understand more about Unity.

> Play above or [here](https://projects.ant.lgbt/unity-exportCamera/), or
> check out on [Gitlab](https://gitlab.com/ConfusedAnt/unity-demo-project).
