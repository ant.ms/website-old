---
title: "Program: Discord Chat Exporter Archiver"
date: 2020-02-26T13:10:17+01:00
draft: false
toc: false
categories:
- Developement
tags:
- script
- bash
- Discord
- Developement
icon: https://gitlab.com/uploads/-/system/project/avatar/17083403/0_FDdiWdrriXPKGNyf.png?width=64
---

So.. this started of with actually having a use case for this software (which is how it should be more often, tbh). We had the issue that we had an old discord server that we wanted to archive, the servers channels where exported with [Discord Chat Exporter](https://github.com/Tyrrrz/DiscordChatExporter).

## Usage
> here, I'm assuming you already used the Chat Exporter to export HTML-Pages

Download *script.sh* from the repo and copy it into an empty folder. Make sure it is marked as an executable and then execute it.

Follow the scripts instructions and if everything works, you should end up with a working localiced copy of your chat.

Download [on Gitlab](https://gitlab.com/ConfusedAnt/discord-chat-exporter-to-local-filesystem)
