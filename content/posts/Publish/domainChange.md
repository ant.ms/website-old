---
title: "A change of domain"
date: 2020-02-04T17:36:47+01:00
draft: false
toc: false
categories:
- Publish
tags:
- update
- domain
---

As you might know (probably not because basically no one is reading this side) this Website was known as [1Yanik3.com](https://1yanik3.com). I recently acquired a new domain ([ant.lgbt](https://ant.lgbt)) and I'm planning on running my website on. If you want to ask why, then don't because there is literally no reason why except it being for fun (and I really wanted an .lgbt domain).
