---
title: "DE: Abschlussarbeit - GamePi-Tablet"
date: 2019-03-27T11:36:47+01:00
draft: false
toc: true
categories:
- Object
- Documents
- Publish
tags:
- work
- German
- school
- Document
- GamePi
- Engineering
- RaspberryPi
---
This document is the documentation of one of my school projects.
You can download the Design-Files here: <br>
**stl:** [Part1](https://stored.ant.lgbt/files/GamePi-Part1.stl), [Part2](https://stored.ant.lgbt/files/GamePi-Part2.stl) <br>
**online:** [OnShape](https://cad.onshape.com/documents/0c50281a8b26b3f78908ffd1/w/1d20597d1fdd82ccd1ed67c1/e/baef79fef3d09c4a0932a417), Thingiverse <br>
**documents:** [tarball](https://stored.ant.lgbt/files/GamePi-Tablet.tar.gz)

![](https://stored.ant.lgbt/img/GamePi-Tablet.png)

## Dokumentation
Mir fiel es nicht sonderlich schwer mich für ein Projekt zu entscheiden. Was mir jedoch schwer fiel war die Umsetzung dieses Projektes, denn ich hatte noch nicht viel Erfahrung mit dem designen von einem so komplexen 3D-Objekt als ich mit diesem Projekt anfing. Durch das Projekt lernte ich einiges in diesem und anderen Bereichen dazu. Es traten, besonders gegen Ende, einige Probleme auf die ich nicht erwartet hatte.

Meine Arbeitsdateien stehen unter Verwendung der **GNU General Public License** auf meiner Website (**[www.1yanik3.com](http://www.1yanik3.com/)**) zum Download zur Verfügung.

## Arbeitsprozess
Da ich bereits zu Beginn bereits einige Dinge über ein solches Projekt wusste, konnte ich mir eine ungefähre Vorstellung machen was ich alles machen müsste. Leider lief nicht immer alles nach Plan und es traten einige unerwartete Komplikationen auf.

Ich begann damit eine Skizze anzufertigen wie das Projekt am Ende aussehen soll und welche Funktionen es haben wird. Danach begann ich damit das Design in OnShape (ein CAD-Programm) zu verwirklichen. Mutmasslich gesehen war dies der schwerste Arbeitsschritt des Projektes, weil ich bis dahin lediglich wenig Erfahrung über das 3D-Modellieren besass. Um mein Wissen in diesem Bereich zu erweitern, erstellte ich in meiner Freizeit einige weitere Modelle und Geräte. Dieses 3D-Modell musste ich während des Arbeitsprozesses mehrmals anpassen, um die technischen Spezifikationen zu erfüllen. Auch recherchierte ich sehr viel über mögliche Software-Lösungen und Hardware-Komponenten die mein Projekt funktional machen sollten.

Die Software war ein weiterer Schwachpunkt. Ich wollte unbedingt RetroPie verwenden, da sie einige der wenigen bekannten Möglichkeiten ist und eine grosse Auswahl an Software unterstützt, dazu zählen zum Beispiel: diverse Emulatoren, Steam-Streaming, Kodi und diverse andere Linux-ARM-kompatible Spiele und Programme. Da Retropie leider (noch) nicht für den Raspberry Pi 3A+ – den ich verwenden möchte – verfügbar ist. Deshalb musste ich sie manuell installieren und einige Teile davon auch manuell kompilieren. Ich muss dazu ja eigentlich nicht sagen, dass es ziemlich lange dauerte. Später musste ich jedoch zu einem Raspberry Pi 3B+ wechseln, da die Software sich weigerte anstandsgemäss zu funktionieren.

Die Elektronik war ein weiteres Debakel. Die Joysticks waren eine zum Scheitern verurteilte Idee. Anfangs dachte ich noch, dass ich die Joysticks per GPIO anschliessen könnte. Ich bemerkte jedoch schnell das dies doch komplizierter werden würde als gedacht. Deshalb versuchte ich es mit dem Teensy, genauer dem Teensy++ 2.0. Ich fand ein Projekt von Adafruit, das auch den Namen dieses Projektes inspiriert hat. In diesem Projekt verwendeten sie einen Tennsy (genauer den Teensy LC) um die analogen Joystick-Signale in digitale umzuwandeln und über USB an den Pi zu senden. Aus mir unbekannten Gründen funktioniert das jedoch bei meinem Projekt nicht. Leider fing dann auch die Zeit langsam an knapp zu werden und so entschied ich die Joysticks einfach wegzulassen. Dies hat die Folge, dass einige Funktionen wie zum Beispiel das Spielen von «moderneren» Spielen schwer werden könnte aber ich musste einen Weg finden dieses Projekt zu beenden. Zudem habe ich einige Spiele gefunden die immer noch mit einem D-Pad zurechtkommen.

Während meiner gesamten Arbeit, schrieb ich all meine Erfolge (und Misserfolge) in diesem Projektjournal auf um anderen davon zu berichten in der Hoffnung, dass die gleichen Fehler nicht mehr begangen werden.

## Reflexion
Ich habe mir zu grosse Ziele gesetzt und musste mich auch manchmal zurückhalten. Trotzdem habe ich viel dazugelernt und konnte die meisten auftauchenden Probleme beheben oder umgehen.

Falls ich wieder so ein Projekt starten sollte, sollte ich jedoch einige Dinge vereinfachen anstatt immer das aktuell beste einbauen zu wollen. Zum Beispiel würde ich mich vermehrt auf die Hardware konzentrieren als auf die Software und versuchen weniger komplexe Grundlagen (z.B. gpionext) von Beginn an zu verwenden.  Als Folge dessen blieb mir nun unter anderem keine Zeit und Mittel das Tablet batteriebetrieben zu machen.

## Fazit
Am Projektunterricht gefiel mir, dass ich frei entscheiden kann was man machen möchte. Man kann seine eigenen Ideen umsetzen und lernt dabei auch selbständiges Denken.

Ich bin ziemlich sicher, dass selbst Aussenstehende mit wenig bis gar keiner Erfahrung in Elektronik haben, den Aufwand verstehen werden der mit diesem Projekt verbunden war. Diejenigen die jedoch eine Ahnung haben werden wahrscheinlich nicht sehr beeindruckt sein.

Trotz den vielen Problemen und bleibenden Verbesserungsmöglichkeiten die dieses Projekt aufgebracht hat, finde dieses Projekt durchaus gelungen, da ich einiges über 3D-Modellierung und Elektronik gelernt habe. Auch besitze ich nun ein funktionierendes Tablet, dass ich auch verwenden kann. Da das Tablet auf Linux basiert wird es unzweifelhaft auch noch sehr lange betriebsfähig bleiben.

Falls ich wieder so etwas machen würde, würde ich auf jeden Fall anders vorgehen aber ich halte das Projekt trotzdem für gelungen

## Projektjournal

### 31.10.2018 – Brainstorming
Heute erstellte ich eine Skizze des Tablets. Das Tablet besteht, wie bereits erwähnt, aus einem 3D-Gedruckten Gehäuse, dieses wird nach hinten dicker, um einen möglichst dünnen Grundriss zu erlauben. Zudem ist mir aufgefallen, dass lediglich 2 Shoulderbuttons (R/L) auf dem Rand des Gerätes Platz finden werden. Ausserdem muss ich mir noch überlegen, wo ich die 4 Menü Knöpfe platziere.

Als Hausaufgabe werde ich die Skizze wiederholt aufzeichnen und die benötigten Einzelteile bestellen, vielleicht kann ich bereits nächste Woche mit dem Arbeiten und modellieren des Tablets beginnen.

### 14.11.2018 – Grundbausteine setzen
Da die benötigten Einzelteile noch nicht eingetroffen sind, habe ich an einem groben Entwurf gearbeitet. Dafür habe ich die Software OnShape genutzt. Leider konnte ich keine Messungen der eigentlichen Einzelteile anstellen, trotzdem habe ich einige Erkenntnisse gewonnen und bin nun auch deutlich vertrauter mit der Software, jedoch immer noch nicht sehr erfahren.

Zusätzlich habe ich heute die „Vereinbarung Abschlussarbeit“ ausgefüllt. Ich bin jedoch mit dem Text noch nicht vollständig zufrieden.

Nächste Woche sollten die wichtigsten oder zumindest einige der Einzelteile dann eingetroffen sein, dann kann ich mit dem technischen Prototypen beginnen und die richtigen Abmessungen der Einzelteile ausmessen und in das OnShape Dokument eintragen.

### 21.11.2018 –Erweiterungen
Zuerst habe ich die Abschlussarbeits-Vereinbarung beendet, ich bin jetzt deutlich glücklicher mit dem Text. Ebenfalls habe ich das Budget korrigiert. Im Verlauf der vorangegangenen Woche sind auch die wichtigsten der Einzelteile eingetroffen, ich kann somit nun beginnen die richtigen Masse einzugeben. Ich konnte ausserdem noch am Design weitermachen, vielleicht mache ich bereits einen Testdruck der obersten Planen.

Das Design nimmt nun langsam an Gestalt an und sieht auch so aus als ob es ein cooles Tablet-Design werden würde.

### 28.11.2018 – Erster Prototyp
Als Hausaufgabe druckte ich den Prototyp (wie im letzten Eintrag angekündigt) aus. Ich merkte sofort, dass ich die Joysticks in die obere Hälfte versetzen müsse. Heute habe ich somit in OnShape das Sketch überarbeitet, Menü und Start Knöpfe hinzugefügt und etwas Form hinzugefügt.

Mir kam auch noch die Idee, dass ich das Tablet als Fernsteuerung für z.B. einen Roboter verwenden könnte. Dieser könnte, dass über eine am Roboter angebrachte Kamera Live-Bilder übertragen die das Tablet dann anzeigt, zusammen mit Sensor Metadaten wie Geschwindigkeit und Akkuladung. Die Steuerung über die Joysticks wäre sehr intuitiv und genau. Kommunizieren könnte das Tablet, mit dem Roboter, über Bluetooth und/oder WLAN. Ich ziehe auch in Betracht einen Infrarot oder Radio Transceiver einzubauen um diese Funktion zu unterstützen, dies würde die Verzögerung dramatisch verringern.

Ich werde erneut einen Prototyp Drucken, wenn auch nur von dem Hauptteil, weil dies sonst eine Materialverschwendung wäre.

### 21.11.2018 – Erkenntnisse
Als Hausaufgabe druckte ich erneut den Prototypen aus und merkte bereits, dass ich das Tablet ca. 3 mm dicker machen muss. Zudem hatten die Joysticks zu wenig Spielraum. Die Frontplatte was zudem zu dick, man konnte deshalb nicht die Knöpfe drücken, da sie zu weit unten waren.

Ich habe zudem die Stabilität-Stützen für das Display angebracht und den Stromverbrauch des Displays gemessen, dieser beträgt zwischen 0.8 und 1 Ampere. Dies führte zur Anmerkung, dass mit meinen zuvor geplanten Batterien ich lediglich eine Akkulaufzeit von 1 bis 2 Stunden erhalten werde. Um dem entgegenzusetzen, habe ich bereits 4 weitere Batterien gekauft und hoffe diese dann später korrekt unterbringen zu können ohne das Tablet bedeutend dicker machen zu müssen. Im schlimmsten Fall muss ich eventuell auf USB-Powerpacks zurückgreifen.

Mir ist ebenfalls aufgefallen, dass für die Dock-Funktionalität wahrscheinlich kein Platz sein wird, da ich mein Tablet nicht noch dicker machen möchte.

### 12.12.2018 – OSMC
Heute recherchiere ich die Software, die dann auf dem Tablet verwendet wird. Ich habe mich nun für OSMC entschieden, anstatt wie zuvor gedacht RetroPie.

OSMC wurde eigentlich für Fernseher entwickelt, kann aber auch auf Tablets bedient werde allen, obwohl einiges an Konfigurationen nötig sein werden. OSMC ist also vereinfacht gesagt eine Verschmelzung von Raspbian und Kodi, somit steht mir eine Reihe von Software zur Verfügung, die dann einfach über ein Simples GUI gestartet werden kann.

Kodi unterstützt von Haus aus eine Reihe an Spielen, diese werden dann auch auf dem Tablet selbst laufen. Für alle weiteren Spiele benötige ich extra Software, so zum Beispiel das Add-on: „Steam Launcher“ welches mir erlaubt Spiele, sofern eine ausreichende Verbindung verfügbar ist, die auf meinem PC laufen auch auf meinem Tablet zu Spielen.

Ebenfalls möchte ich einen integrierten USB-Stick einbauen, auf den ich dann z.B. Filme/Bücher/Serien/Fotos/… abspeichern kann und wenn ich unterwegs bin über einen von Tablet erzeugten WLAN-Hotspot oder direkt auf dem Tablet zugreifen kann.

### 19.12.2018 – Elektroniktest
Heute habe ich versucht das Display an den Raspberry Pi anzuschliessen, dies hat auch funktioniert und ich erhielt eine Auflösung von 1080p mit 60 FPS. Leiter klappte die Verbindung mit dem GPIO-Joystick nicht so gut wie geplant, mir war es nicht möglich diesen korrekt einzubinden. Es stellt sich heraus, dass dies ein grösseres Problem werden könnte als gedacht.

Ich werde dies Zuhause versuchen, sollte der Versuch fehlschlagen bin ich gezwungen entweder ein USB-Gamecontrollerboard zu verwenden und meine Joysticks dann daran zu befestigen oder RetroPie als Backbone zu installieren.

### 19.12.2018 – Programmsammlung
Ich habe mich aber über die GIPO-Steuerung erkundigt. Als Resultat dazu wäre es mir zwar möglich die Joysticks an OSMC anzuschliessen würde aber sehr kompliziert werden. Ich entschied mich also wieder mich an RetroPie zu wenden. Heute recherchierte ich über RetroPie und wie man dort Funktionen hinzufügen kann.

Um die Multimedia-Features dennoch nutzen zu können, kann ich Kodi manuell installieren oder auch eine andere z.B. Elektron basierte App (z.B. Plex) verwenden, der RaspberryPi ist leider nicht besonders schnell, aber für einfachere Apps sollte es ausreichen. Vieleicht gelingt es mir auch einige Linux Spiele (z.B. SuperTuxCart) ohne Streaming zu spielen.

### 19.12.2018 – Einrichtung
Heute habe ich das Tablet mit RetroPie ausgestattet und versucht einen der Joysticks anzuschliessen. Mir ist auch ein Designfehler bei der Displayhalterung aufgefallen, welches auf Dauer das «Displaykabel» beschädigt hätte. Zudem fiel mir auf, dass die Menü-Knöpfe auf der rechten Seite des Tablets mit dem Display kollidieren, ich werde sie also noch verschieben müssen.

Die Installation war nicht erfolgreich, ich werde es als Hausaufgabe Zuhause erledigen.

### 19.12.2018 – GPIO-Komplikationen
Die Installation ist gelungen, jedoch bereitet die Interpretierung der GPIO Pins Komplikationen. Das WLAN erlaubt mir keinen Remote-Zugang auf den Pi was ebenfalls nicht gerade hilfreich ist.

Der Raspberry Pi besitzt 26 verschiedene GPIO-Pins davon benötige ich mindestens 22. Einige der Pins sind jedoch «pull-up» Pins und somit für gaming nicht sehr geeignet, diese werde ich für die Meta-Buttons (die Kleinen) verwenden da diese nicht besonders oft genutzt werden. Die Tatsache, dass er lediglich über 3 gegenzeichneten PWM-Pins verfügt könnte noch Probleme verursachen.

Da das Schul-WLAN lediglich ein Gerät pro Schüler und kein SSH erlaubt muss ich ab nächstem Mal dann Zuhause arbeiten. Heute erweitere ich also noch meine Dokumentation.

### 19.12.2018 – TEENSY++ 2.0
Die Komplikationen mit den Joysticks werden nicht besser, ich denke darüber nach sie einfach durch D-PADs auszutauschen. Jedoch fand ich ein Lichtschimmer: Das TEENSY 2.0 Board. Hoffentlich ist es in der Lage die Joysticksignale als USB-Signale weiterzugeben. Ich werde diesen also noch bestellen und hoffe, dass das Problem damit endlich gelöst ist.

### 20.02.2019 – Designänderung
Der Teensy scheint nicht zu funktionieren. Auch nachdem ich es nach etwa einer Stunde es endlich geschafft hatte den Code korrekt zu kompilieren nahm der Raspberry Pi ihn nicht an.
Da mir langsam die Zeit davonläuft muss ich leider die Joysticks weglassen, dies ist zwar bedauerlich jedoch werde ich damit mein Projekt rechtzeitig fertigstellen.

Ich habe nun die Joysticks durch das D-Pad und die ABXY ersetzt, welche ich dann mithilfe von GPIO-Next ansteuern kann. Somit benötige ich den Teensy nicht mehr und kann einiges an Zeit sparen.

Mein 3D-Drucker ist immer noch defekt, da der Versandhändler das falsche Ersatzteil gesendet hat. Ich bin jedoch zuversichtlich, dass ich ihn jedoch in den nächsten Wochen wieder reparieren kann.

### 27.02.2018 – Verkabelung
Mein 3D-Drucker ist nun wieder funktional. Heute habe ich die GPIO-Header entfernt und ca. die Hälfte der Buttons verkabelt. Ich habe dieses Wochenende noch Zeit, werde also nächste Woche mit dem Projekt fertig (zumindest teilweise). Ich habe nicht erwartet, dass die Verkabelung so viel Zeit beanspruchen würde. So langsam sieht auch die interne Technik aus wie ein fertiges Projekt.

### 06.03.2018 – Verkabelung 2 und Erfolg
Ich habe nun das D-Pad, die ABXY-Buttons und die Menü-/Start-Buttons verbunden. Der Prototyp ist nun funktional. Ich habe zudem noch das Gehäuse optimiert und drucke nun den (hoffentlich) finalen Prototyp aus. Ich bin zuversichtlich, dass ich bis nächster Woche ein funktionsfähiges Tablet erhalte.

Mir fielen jedoch einige Probleme mit der Stromversorgung des Displays auf, es scheint deutlich mehr Strom zu verbrauchen als angegeben. Dies könnte zu zukünftigen Problemen führen.
Zudem habe ich an der Dokumentation weitergearbeitet, die jetzt deutlich mehr Inhalt besitzt.

### 13.03.2018 – Endspurt
Die Software macht mir weiterhin Probleme. Ich befestigte alle verbleibenden Knöpfe, montierte das Display und konfigurierte das Display. Den Rpi 3 A+ musste ich zu einem 3 B+ ersetzen, was weniger problemlos verlief als zuerst angenommen, da der Raspberry Pi 3B+ dasselbe GPIO-Layout besitzt wie der 3A+ was bedeutet das ich einfach die Kabel an derselben Stelle wie beim 3A+ wieder befestigen kann.

### 20.03.2019 – Letzte Änderungen
Heute habe ich das Tablet fertiggestellt. Ich habe zudem auch die Backplate neu designt.

Mein Projekt nähert sich nun schlagartig dem Ende. Ich befestigte die neu gedruckte Rückplatte und vervollständige die Hardware.

Ich arbeitete zudem auch an der Dokumentation, die nun beinahe abgeschlossen ist. Unteranderem Fügte ich die Bilder zur Bildergalerie hinzu.

## Bildergalerie und Schemas

![Front-Plate](https://stored.ant.lgbt/img/GamePi-figure1.png)<br>
![Prototyp](https://stored.ant.lgbt/img/GamePi-figure2.png)<br>
![Bootscreen am fertigen Produkt](https://stored.ant.lgbt/img/GamePi-figure3.png)

















