---
title: "About"
date: 2019-12-04T00:00:00+01:00
draft: false
toast: true
updateScript: true
---

Welcome to [ant.lgbt](https://ant.lgbt/) this is my personal Website where I post random ~~mostly stupid~~ stuff while pretending to be a professional... Anyways, thank you for visiting and enjoy your stay on this little island in the depths of the global Internet :D

Btw, this website supports multiple color-themes. Click the adjust icon in the menubar to change between <a onclick="setTheme(this.innerText)">dark</a>, <a onclick="setTheme(this.innerText)">light</a> and <a onclick="setTheme(this.innerText)">nord</a> color schemes, or click on the links to change it.

If you're looking for my hosting-list, go [here](/hosting/).

You may be wondering why I chose the domain [ant.lgbt](ant.lgbt)... The answer isn't actually that complicated: First, .lgbt domains exist and as a fellow gay I just want to have one. Second, my username has been *ConfusedAnt* for quite a while now and so it would make sense to use ant.lgbt as the domain so I could have [confused@ant.lgbt](mailto:confused@ant.lgbt) as my Email (which I personally find hilarious ~~don't judge me~~).

<script async>
document.getElementsByTagName("h1")[0].innerHTML = document.getElementsByTagName("h1")[0].innerHTML + " <i class=\"ri-contacts-fill\"></i>";
function LinuxPopup() {
    alert('I\'d just like to interject for a moment. What you\'re referring to as Linux, is in fact, GNU/Linux, or as I\'ve recently taken to calling it, GNU plus Linux. Linux is not an operating system unto itself, but rather another free component of a fully functioning GNU system made useful by the GNU corelibs, shell utilities and vital system components comprising a full OS as defined by POSIX.\n\nMany computer users run a modified version of the GNU system every day, without realizing it. Through a peculiar turn of events, the version of GNU which is widely used today is often called \"Linux\", and many of its users are not aware that it is basically the GNU system, developed by the GNU Project.\n\nThere really is a Linux, and these people are using it, but it is just a part of the system they use. Linux is the kernel: the program in the system that allocates the machine\'s resources to the other programs that you run. The kernel is an essential part of an operating system, but useless by itself; it can only function in the context of a complete operating system. Linux is normally used in combination with the GNU operating system: the whole system is basically GNU with Linux added, or GNU/Linux. All the so-called \"Linux\" distributions are really distributions of GNU/Linux.')
}
</script>

## About me

<img src="https://stored.ant.lgbt/img/me-min.jpg" style="float:left;margin-right:13px;width:15rem;height:20rem;object-fit:cover" onclick="showImageToast('https://stored.ant.lgbt/img/me.jpg')">

Hey there, my name is Yanik Ammann, also known as 1Yanik3 or ConfusedAnt (yes, I'm very confused basically all of the time). I'm a very gay nerd and got some experience with coding stuff like websites or applications (also some simple games ~~that might not have turned out as fun as I wish they had~~). Currently I'm doing an apprenticeship at Siemens as an Application Developer (I really love it so far, it's a lot of fun and you learn A LOT). Btw, Linux is awesome, that's why I absolutely love it and use it every day ~~(if you like Windows, FIGHT ME!!!)~~, and I'm also interested in programming and science ~~and boys~~.

<div style="clear:both"></div>

**You can find me at:**
<table style="margin-left:8%">
<tr>
<td style="border:0px" class="floatOnMobile"><a href="https://gitlab.com/ConfusedAnt"><i class="ri-gitlab-fill"></i> Gitlab: Yanik Ammann</a></td>
<td style="border:0px" class="floatOnMobile"><a href="mailto:confused@ant.lgbt"><i class="ri-mail-fill"></i> Email: confused@ant.lgbt</a></td>
</tr>
<tr>
<td style="border:0px" class="floatOnMobile"><a href="https://www.reddit.com/user/1Yanik3"><i class="ri-reddit-fill"></i> Reddit: u/1Yanik3</a></td>
<td style="border:0px" class="floatOnMobile"><a onclick="copyToClipboard('Yanik#0704')"><i class="ri-discord-fill"></i> Discord: Yanik#0704</a></td>
</tr>
</table>

I created and hosted a lot of websites since I started programming, you can look at a short list of them [here](/hosting/). All my other projects have posts in the [Blog section](/posts/) of this website. Some of my Project haven't been published on my website yet/ever, if you want to check those out, visit [my GitLab](https://gitlab.com/ConfusedAnt). Have fun exploring my website or wherever it leads you, I'd love to hear your feedback if you have any!

I'm fluent in both English and German, therefor, some of the posts on this site are written in German. Those German posts are marked with the DE-Prefix.
[Categories](/categories/) are available and marked with prefixes and, of course, every post is [tagged](/tags/) and allows you to quickly find posts of similar topics. I hope you enjoy your visit on my site. No matter how long you plan on staying here, have a nice day and enjoy your live :)

### Biggest Projects

<div style="width: 100%;">
    <div class="floats" onclick="window.location.href='/posts/developement/jrnl-web/'">
        <div class="pride" style="background-image: url('https://files.ant.lgbt/icons/jrnlWeb.png')"></div>
        <div class="pridetext">
            <a href="/posts/developement/jrnl-web/" class="needwhite">Program: Jrnl-Web (PHP)</a>
        </div>
        <div class="pridedate">
            <span class="post-day">05.2020</span>
        </div>
    </div>
    <div class="floats" onclick="window.location.href='/posts/developement/antchat/'">
        <div class="pride" style="background-image: url('https://files.ant.lgbt/icons/antChat.png')"></div>
        <div class="pridetext">
            <a href="/posts/developement/antchat/" class="needwhite">Program: AntChat (PHP)</a>
        </div>
        <div class="pridedate">
            <span class="post-day">03.2020</span>
        </div>
    </div>
    <div class="floats" onclick="window.location.href='/posts/developement/games/stragegy-like-camera-demo/'">
        <div class="pride" style="background-image: url('https://files.ant.lgbt/icons/strategyLikeCameraDemo.png')"></div>
        <div class="pridetext">
            <a href="/posts/developement/games/stragegy-like-camera-demo/" class="needwhite">Game: 'Stragegy-Like-Camera' (Unity)</a>
        </div>
        <div class="pridedate">
            <span class="post-day">06.2020</span>
        </div>
    </div>
    <div class="floats" onclick="window.location.href='/posts/developement/juliheart/'">
        <div class="pride" style="background-image: url('https://files.ant.lgbt/icons/leo.jpg')"></div>
        <div class="pridetext">
            <a href="/posts/developement/juliheart/" class="needwhite">Project Leo (Arduino)</a>
        </div>
        <div class="pridedate">
            <span class="post-day">07.2020</span>
        </div>
    </div>
</div>
<div style="clear:both"></div>

### Operating System

<img src="https://stored.ant.lgbt/img/manjaro-logo.png" style="float:right;margin-left:13px;margin-top:8px;width:13em;opacity: 0.8">

In order to get this out of the way: I use <a href="javascript:void(0)" onclick="LinuxPopup()">Linux</a> as my main operating system. Still not completely sure what desktop I should settle with or what distro fits me best, but at the moment, I'm using [Manjaro](https://manjaro.org/) with the [Plasma desktop (KDE)](https://kde.org/) mainly because of it's really good way of handling themes (I use the [Nord theme](https://www.nordtheme.com/), which by the way is great, you should check it out). Though I don't like the fact that [qt](https://www.qt.io/) is at least somewhat propietary. I used to use Gnome for everything, because of that, most of my recent applications are written for GTK3 and use headerbars (I really like the Gnome design language and even-though I can agree that there are better things at times, in my opinion it's a very good compromise between functionality, performance and looks).

### Hardware

I use multiple PC's, of course. But my main "Workstation" uses an *AMD Ryzen 3600 @ 6x 3.6GHz* and a *RTX 2080*. For storage, I have a *512 GB M.2 SSD* and two *4TB HDDs* for my data storage needs. I've been upgrading my CPU recently, I used to have a *Intel Core i5-8600K @ 6x 4.7GHz* with watercooling but I switched away from that because I wanted to upgrade to a faster CPU and to air cooling because it's a lot less of a hassale and generally, it's actually quieter.

My Server (which you're currently accessing this website on btw) has a *1TB M.2 SSD* from Samsung, which was donated/given to me by a friend of mine and four *4TB HDDs* which are combined using LVM to form a 16TB disk which is my main storage space.

<table style="margin-left:10%">
<tr>
<td style="border:0px" class="floatOnMobile"><i class="ri-cpu-fill"></i> CPU: <span id="axxivServerCPU">Offline</span></td>
<td style="border:0px" class="floatOnMobile"><i class="ri-database-fill"></i> Memory: <span id="axxivServerMemory">Offline</span</td>
</tr>
<tr>
<td style="border:0px" class="floatOnMobile"><i class="ri-hard-drive-2-fill"></i> Root: <span id="axxivServerStorage">Offline</span></td>
<td style="border:0px" class="floatOnMobile"><i class="ri-hard-drive-fill"></i> /mnt: <span id="axxivServerStorageMnt">Offline</span></td>
</tr>
</table>

### Licenses

All of my projects are free to use and modify, so feel to fork them or something. If you want to republish them, add a link to my website and/or my GitLab or email, and, if possible, inform me, I'd love to find out what you plan on doing with it. If you have any questions or feedback, send me an email to [confused@ant.lgbt](mailto:confused@ant.lgbt) or contact me on Discord. If no license is specified, you can generally assume that it's GPL3.



<!-- Live update -->
<script>
    setInterval(tickLiveUpdate , 750);
</script>
<style>
    a:hover {
        text-shadow: unset;
        color: var(--text);
    }
    td {
        border: 0px !important;
        padding: 0 !important;
    }
    a.needwhite {
        color: var(--text) !important;
    }
    .pridedate {
        margin-top: 17px;;
    }
</style>