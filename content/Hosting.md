---
title: "Hosting"
date: 2019-12-16T18:20:05+01:00
draft: false
updateScript: true
---

My hosting solution is getting more and more advanced (aka: unnecesairly complicated). Currently I have one main server where I have different docker-containers (most of the time at least).

> Live-Performance numbers (that you can see underneeth), are made possible by my [System Monitor API](/posts/developement/system-monitor-api/)

## Network/Hardware

![Network Diagram](https://cdn.ant.lgbt/img/Julianik.png)

This is very WIP (work in progress), details not completely accurate yet.

<script> document.getElementsByTagName("h1")[0].innerHTML = document.getElementsByTagName("h1")[0].innerHTML + " <i class=\"ri-server-fill\"></i>"; </script>

### PfSense

My router is a custom built (and completely overkill) [PfSense](https://www.pfsense.org/)-Box. It's, by far, one of the coolest things I own and allows me to do so many things with my network. It also has a network-wide-adblock (which doesn't always work sadly) on it.

I highly recommend this kind of project, because it has a lot of use cases.

**DISCLAIMER:** Yes, I know that this system is completely and utterly overpowered. On high usage it sits at arround 2% to 5% CPU-Usage. It wasn't that much more expensive than the next cheaper option though, and allows me to - in the future - potentially add something like a [Steam Cache](https://hub.docker.com/u/steamcache/) and support many simultanious VPN-Clients.

### Axxiv Server

This server is the "gateway" that every publically accessible service goes through. It hosts things like this website, some personal services and much more. It serves as my main Workhorse. I'm thinking about migrating those "personal services" to Epsilon though.

<table style="margin-left:10%">
<tr>
<td style="border:0px" class="floatOnMobile"><i class="ri-cpu-fill"></i> CPU: <span id="axxivServerCPU">Offline</span></td>
<td style="border:0px" class="floatOnMobile"><i class="ri-database-fill"></i> Memory: <span id="axxivServerMemory">Offline</span</td>
</tr>
<tr>
<td style="border:0px" class="floatOnMobile"><i class="ri-hard-drive-2-fill"></i> Root: <span id="axxivServerStorage">Offline</span></td>
<td style="border:0px" class="floatOnMobile"><i class="ri-hard-drive-fill"></i> /mnt: <span id="axxivServerStorageMnt">Offline</span></td>
</tr>
</table>

It is a little bit overkill for what I use it for, but I don't see that as a bad thing necesairly. It's also the server, which I'm experimenting most on. The Hardware is actually my old personal [Workstation](http://localhost:1313/about/#hardware).

### FreeNAS

This is my main NAS-System, it contains a 20TB Raid-z5 (6x 4TB, one drive redundancy) which is my main Storage array for things like videos, movies and much more. It also contains two 2TB NVME-SSDs that are striped together to give me 4TB of very high speed storage, which I can access over it's 10Gbit connection to my Workstation (only 1Gbit to the rest of the Network for now).

It also hosts my Nextcloud and even two Jellyfin-instances (as this server, is the most powerful server I own for some reason).

The Operating System, that it is running on, is [FreeNAS](https://www.freenas.org/), because it's honestly the only option that both meets my requirements and is FOSS (FreeOpenSourceSoftware). It is constantly backed up to Epsilon server and also to OneDrive, because I get 1TB of storage there for free from my school.


### Epsilon

This is the newest addition to my server-collection. It doesn't really have a use-case just yet and may never actually have one. I have used it in the past to mine Monero, because I don't have any other tasks for it just yet.

<table style="margin-left:10%">
<tr>
<td style="border:0px" class="floatOnMobile"><i class="ri-cpu-fill"></i> CPU: <span id="epsilonServerCPU">Offline</span></td>
<td style="border:0px" class="floatOnMobile"><i class="ri-database-fill"></i> Memory: <span id="epsilonServerMemory">Offline</span</td>
</tr>
<tr>
<td style="border:0px" class="floatOnMobile"><i class="ri-hard-drive-2-fill"></i> Root: <span id="epsilonServerStorage">Offline</span></td>
<td style="border:0px" class="floatOnMobile"><i class="ri-hard-drive-fill"></i> /mnt: <span id="epsilonServerStorageMnt">Offline</span></td>
</tr>
</table>

I have plans for this though, do kind of act as a backup for everything, and maybe even as a fall-back webserver for all the websites, that I host.

---

## Webhosting

In order to get this out of the way: if you're interested in me hosting and/or creating a website for you, message me under [info@1yanik3.com](mailto:info@1yanik3.com) for more information or questions about this (you can also message me on Discord if you prefer). Every Website I host is encrypted with SSL.

I like creating, designing and coding websites, therefore it's not a suprise that I created and hosted a lot of websites since I started programming (yes, I'm aware that HTML isn't a programming language, shush), some of them are listed down below.

### Main Sites I created, hosted and maintain:
| Site-Name &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | **URL** &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | Type &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | Engine &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | 
| --- | --- | --- | --- | 
| ConfusedAnt | [ant.lgbt](https://ant.lgbt/) | Blog | <i class="fab fa-markdown"></i> Hugo |
| ConfusedCloud | [cloud.ant.lgbt](https://cloud.ant.lgbt/) | Cloud | <i class="fas fa-cloud"></i> Nextcloud |
| Julien Acker | [julienacker.dev](https://julienacker.dev/) | Cute Page | <i class="fab fa-html5"></i> HTML |
| Alex Heimservice | [alex-heimservice.ch](http://www.alex-heimservice.ch/) | Business | <i class="fab fa-wordpress-simple"></i> Wordpress | 
| La’a Kea | [laakea.ch](http://www.laakea.ch) | Business | <i class="fab fa-wordpress-simple"></i> Wordpress |

### Websites I created but am no longer working on or maintaining:
| Site-Name &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | **URL** &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | Type &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | Engine &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; |
| --- | --- | --- | --- | 
| Visual-Gallery &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | [www.visual-gallery.ch](https://www.visual-gallery.ch/de/) | Business | <i class="fab fa-wordpress-simple"></i> Wordpress |
| Paradise To Rent | [www.paradise-to-rent.com](https://www.paradise-to-rent.com/) | Business | <i class="fab fa-wordpress-simple"></i> Wordpress |

###  My personal Subdomains and hosted projects
| Site-Name &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | **URL** &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | Type &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | Engine &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; |
| --- | --- | --- | --- | 
| [GifAnt](/posts/developement/gifant/) | [gif.ant.lgbt](https://gif.ant.lgbt) | Project | <i class="fab fa-html5"></i> HTML |
| [AntChat](/posts/developement/antchat/) | [projects.ant.lgbt/chat](https://projects.ant.lgbt/chat) | CDN | <i class="fab fa-php"></i> PHP |
| [AntGallery](/posts/developement/antgallery/) | [projects.ant.lgbt/simple-gallery](https://projects.ant.lgbt/simple-gallery) | CDN | <i class="fab fa-php"></i> PHP |
| [YummyAnt](/posts/developement/yummyant/) | [yummy.ant.lgbt](https://yummy.ant.lgbt) | Project | <i class="fab fa-html5"></i> HTML (PWA) |
| EatAnt | [eat.ant.lgbt](https://eat.ant.lgbt) | Project | <i class="fab fa-html5"></i> HTML (PWA) |
| About Me | [me.ant.lgbt](https://me.ant.lgbt) | CV | <i class="fab fa-html5"></i> HTML (PWA) |
| AntCDN | [files.ant.lgbt](https://cdn.ant.lgbt) | CDN | <i class="fas fa-server"></i> Nginx |
| FitAnt | [fit.ant.lgbt](https://fit.ant.lgbt) | Project | <i class="fab fa-html5"></i> HTML |

<!-- Live update -->

<script>
    setInterval(tickLiveUpdate , 750);
</script>
