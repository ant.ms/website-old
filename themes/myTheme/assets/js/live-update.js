var updateLive = true;

function tickLiveUpdate() {
    if (updateLive)
    // update AxxivServer - CPU
    var request = new XMLHttpRequest();
    request.open('GET', 'https://ant.lgbt/api/cpu', true);
    request.onload = function() {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById("axxivServerCPU").innerText = this.responseText.replace("\n", "") + "%";
        } else {
            document.getElementById("axxivServerCPU").innerText = "Offline";
        }
    }
    request.send();

    // update AxxivServer - RAM
    var request = new XMLHttpRequest();
    request.open('GET', 'https://ant.lgbt/api/ram', true);
    request.onload = function() {
        if (this.readyState == 4 && this.status == 200) {
            var response = JSON.parse(this.responseText);
            document.getElementById("axxivServerMemory").innerText = formatBytes(response.used, 1) + " / " + formatBytes(response.total, 1);
        } else {
            document.getElementById("axxivServerMemory").innerText = "Offline";
        }
    }
    request.send();

    // update AxxivServer - Storage
    var request = new XMLHttpRequest();
    request.open('GET', 'https://ant.lgbt/api/storage/root', true);
    request.onload = function() {
        if (this.readyState == 4 && this.status == 200) {
            var response = JSON.parse(this.responseText);
            document.getElementById("axxivServerStorage").innerText = formatBytes(response.used, 1) + " / " + formatBytes(response.total, 1);
        } else {
            document.getElementById("axxivServerStorage").innerText = "Offline";
        }
    }
    request.send();
    
    // update AxxivServer - Storage Other
    var request = new XMLHttpRequest();
    request.open('GET', 'https://ant.lgbt/api/storage/mnt', true);
    request.onload = function() {
        if (this.readyState == 4 && this.status == 200) {
            var response = JSON.parse(this.responseText);
            document.getElementById("axxivServerStorageMnt").innerText = formatBytes(response.used, 1) + " / " + formatBytes(response.total, 1);
        } else {
            document.getElementById("axxivServerStorageMnt").innerText = "Offline";
        }
    }
    request.send();
}

function formatBytes(bytes, decimals = 2) {
    if (bytes === 0) return '0 Bytes';

    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

    const i = Math.floor(Math.log(bytes) / Math.log(k));

    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}

function onBlur() {
	updateLive = false;
};
function onFocus(){
	updateLive = true;
};

window.onfocus = onFocus;
window.onblur = onBlur;
