# ant.lgbt

> **THIS REPOSITORY MOVED TO [git.ant.lgbt/ConfusedAnt/website](https://git.ant.lgbt/ConfusedAnt/website)**

This is my website, it contains some of my projects and some personal stuff.

All of my projects are free to use and modify, so do whatever you want with them. If you have any questions or feedback to anything on my site, send me an email to [info@1yanik3.com](https://mailto:info@1yanik3.com)
